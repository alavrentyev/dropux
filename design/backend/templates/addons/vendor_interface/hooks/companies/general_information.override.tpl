{if $auth.user_type == "V" && $_REQUEST.dispatch == "companies.update"}
<div class="control-group">
    <label for="elm_company_name" class="control-label cm-required">{__("vendor_name")}:</label>
    <div class="controls">
        <input type="text" name="company_data[company]" id="elm_company_name" size="32" value="{$company_data.company}" class="input-large" />
    </div>
</div>

{if "ULTIMATE"|fn_allowed_for}
{hook name="companies:storefronts"}
<div class="control-group">
    <label for="elm_company_storefront" class="control-label cm-required">{__("storefront_url")}:</label>
    <div class="controls">
    {if $runtime.company_id}
        http://{$company_data.storefront|unpuny}
    {else}
        <input type="text" name="company_data[storefront]" id="elm_company_storefront" size="32" value="{$company_data.storefront|unpuny}" class="input-large" placeholder="http://" />
    {/if}
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="elm_company_secure_storefront">{__("secure_storefront_url")}:</label>
    <div class="controls">
    {if $runtime.company_id}
        https://{$company_data.secure_storefront|unpuny}
    {else}
        <input type="text" name="company_data[secure_storefront]" id="elm_company_secure_storefront" size="32" value="{$company_data.secure_storefront|unpuny}" class="input-large" placeholder="https://" />
    {/if}
    </div>
</div>
{/hook}

{hook name="companies:storefronts_design"}
{if $id}
{include file="common/subheader.tpl" title=__("design")}

<div class="control-group">
    <label class="control-label">{__("store_theme")}:</label>
    <div class="controls">
        <p>{$theme_info.title}: {$current_style.name}</p>
        <a href="{"themes.manage?switch_company_id=`$id`"|fn_url}">{__("goto_theme_configuration")}</a>
    </div>
</div>
{else}
    <input type="hidden" value="responsive" name="company_data[theme_name]">
{/if}
{/hook}

{/if}

{if "MULTIVENDOR"|fn_allowed_for}
    {if !$runtime.company_id}
        {include file="common/select_status.tpl" input_name="company_data[status]" id="company_data" obj=$company_data}
    {else}
        <div class="control-group">
            <label class="control-label">{__("status")}:</label>
            <div class="controls">
                <label class="radio"><input type="radio" checked="checked" />{if $company_data.status == "A"}{__("active")}{elseif $company_data.status == "P"}{__("pending")}{elseif $company_data.status == "N"}{__("new")}{elseif $company_data.status == "D"}{__("disabled")}{/if}</label>
            </div>
        </div>
    {/if}

    <div class="control-group">
        <label class="control-label" for="elm_company_language">{__("language")}:</label>
        <div class="controls">
        <select name="company_data[lang_code]" id="elm_company_language">
            {foreach from=$languages item="language" key="lang_code"}
                <option value="{$lang_code}" {if $lang_code == $company_data.lang_code}selected="selected"{/if}>{$language.name}</option>
            {/foreach}
        </select>
        </div>
    </div>
{/if}


{if !$id}
    {literal}
    <script type="text/javascript">
    function fn_toggle_required_fields()
    {
        var $ = Tygh.$;
        var checked = $('#company_description_vendor_admin').prop('checked');

        $('#company_description_username').prop('disabled', !checked);
        $('#company_description_first_name').prop('disabled', !checked);
        $('#company_description_last_name').prop('disabled', !checked);

        $('.cm-profile-field').each(function(index){
            $('#' + Tygh.$(this).prop('for')).prop('disabled', !checked);
        });
    }

    function fn_switch_store_settings(elm)
    {
        jelm = Tygh.$(elm);
        var close = true;
        if (jelm.val() != 'all' && jelm.val() != '' && jelm.val() != 0) {
            close = false;
        }
        
        Tygh.$('#clone_settings_container').toggleBy(close);
    }

    function fn_check_dependence(object, enabled)
    {
        if (enabled) {
            Tygh.$('.cm-dependence-' + object).prop('checked', 'checked').prop('readonly', true).on('click', function(e) {
                return false
            });
        } else {
            Tygh.$('.cm-dependence-' + object).prop('readonly', false).off('click');
        }
    }
    </script>
    {/literal}

    {if !"ULTIMATE"|fn_allowed_for}
        <div class="control-group">
            <label class="control-label" for="elm_company_vendor_admin">{__("create_administrator_account")}:</label>
            <div class="controls">
                <label class="checkbox">
                    <input type="checkbox" name="company_data[is_create_vendor_admin]" id="elm_company_vendor_admin" checked="checked" value="Y" onchange="fn_toggle_required_fields();" />
                </label>
            </div>
        </div>
    {/if}
{/if}
{if !$runtime.company_id && "MULTIVENDOR"|fn_allowed_for}
<div class="control-group">
    <label class="control-label" for="elm_company_vendor_commission">{__("vendor_commission")}:</label>
    <div class="controls">
    <input type="text" name="company_data[commission]" id="elm_company_vendor_commission" value="{$company_data.commission}"  />
    <select name="company_data[commission_type]" class="span1">
        <option value="A" {if $company_data.commission_type == "A"}selected="selected"{/if}>{$currencies.$primary_currency.symbol nofilter}</option>
        <option value="P" {if $company_data.commission_type == "P"}selected="selected"{/if}>%</option>
    </select>
    </div>
</div>
{/if}




{if "MULTIVENDOR"|fn_allowed_for}
{hook name="companies:contact_information"}
{if !$id}
    {include file="views/profiles/components/profile_fields.tpl" section="C" title=__("contact_information")}
{else}
    {include file="common/subheader.tpl" title=__("contact_information")}
{/if}

<div class="control-group">
    <label for="elm_company_email" class="control-label cm-required cm-email">{__("email")}:</label>
    <div class="controls">
        <input type="text" name="company_data[email]" id="elm_company_email" size="32" value="{$company_data.email}" class="input-large"  disabled="disabled"/>
    </div>
</div>

<div class="control-group">
    <label for="elm_company_phone" class="control-label cm-required">{__("phone")}:</label>
    <div class="controls">
        <input type="text" class="input-large" name="company_data[phone]" id="elm_company_phone" size="32" value="{$company_data.phone}" />
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="elm_company_url">{__("url")}:</label>
    <div class="controls">
        <input type="text" class="input-large" name="company_data[url]" id="elm_company_url" size="32" value="{$company_data.url}" />
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="elm_company_fax">{__("fax")}:</label>
    <div class="controls">
        <input type="text" class="input-large" name="company_data[fax]" id="elm_company_fax" size="32" value="{$company_data.fax}"  />
    </div>
</div>
{/hook}


{hook name="companies:shipping_address"}
{if !$id}
    {include file="views/profiles/components/profile_fields.tpl" section="B" title=__("sd_vendor_fields_warehouse_address") shipping_flag=false}
{else}
    {include file="common/subheader.tpl" title=__("sd_vendor_fields_warehouse_address")}
{/if}

<div class="control-group">
    <label for="elm_company_address" class="control-label cm-required">{__("address")}:</label>
    <div class="controls">
        <input type="text" class="input-large" name="company_data[address]" id="elm_company_address" size="32" value="{$company_data.address}" />
    </div>
</div>

<div class="control-group">
    <label for="elm_company_city" class="control-label cm-required">{__("city")}:</label>
    <div class="controls">
        <input type="text" class="input-large" name="company_data[city]" id="elm_company_city" size="32" value="{$company_data.city}" />
    </div>
</div>

<div class="control-group">
    <label for="elm_company_country" class="control-label cm-required">{__("country")}:</label>
    <div class="controls">
    {assign var="_country" value=$company_data.country|default:$settings.General.default_country}
    <select class="cm-country cm-location-shipping" id="elm_company_country" name="company_data[country]">
        <option value="">- {__("select_country")} -</option>
        {foreach from=$countries item="country" key="code"}
        <option {if $_country == $code}selected="selected"{/if} value="{$code}">{$country}</option>
        {/foreach}
    </select>
    </div>
</div>

<div class="control-group">
    {$_country = $company_data.country|default:$settings.General.default_country}
    {$_state = $company_data.state|default:$settings.General.default_state}

    <label for="elm_company_state" class="control-label cm-required">{__("state")}:</label>
    <div class="controls">
    <select id="elm_company_state" name="company_data[state]" class="cm-state cm-location-shipping {if !$states.$_country}hidden{/if}">
        <option value="">- {__("select_state")} -</option>
        {if $states.$_country}
            {foreach from=$states.$_country item=state}
                <option {if $_state == $state.code}selected="selected"{/if} value="{$state.code}">{$state.state}</option>
            {/foreach}
        {/if}
    </select>
    <input type="text" id="elm_company_state_d" name="company_data[state]" size="32" maxlength="64" value="{$_state}" {if $states.$_country}disabled="disabled"{/if} class="cm-state cm-location-shipping {if $states.$_country}hidden{/if} cm-skip-avail-switch" />
    </div>
</div>

<div class="control-group">
    <label for="elm_company_zipcode" class="control-label cm-required cm-zipcode cm-location-shipping">{__("zip_postal_code")}:</label>
    <div class="controls">
        <input type="text" name="company_data[zipcode]" id="elm_company_zipcode" size="32" value="{$company_data.zipcode}" />
    </div>
</div>

<div class="control-group">
    <label for="elm_company_phone_2" class="control-label cm-required">{__("phone")}:</label>
    <div class="controls">
        <input type="text" class="input-large" name="company_data[w_phone]" id="elm_company_phone_2" size="32" value="{$company_data.w_phone}" disabled="disabled" />
    </div>
</div>
{/hook}
{/if}


{include file="common/subheader.tpl" title=__("sd_vendor_fields_additional_information")}

<div class="control-group">
    <label class="control-label cm-required sd-check-checkboxes"  for="11">{__("sd_vendor_fields_do_you_sell")}:</label>
    <div class="controls">
        <input type="hidden" name="company_data[do_you_sell_do_not_sell_with]" value="" />
        <input type="checkbox" name="11" id="11" checked="checked" style="display: none" value="Y" />
        
        <label class="checkbox inline" for="elm_do_you_sell_do_not_sell_with_1">
            <input type="checkbox" name="company_data[do_you_sell_do_not_sell_with][1]" id="elm_do_you_sell_do_not_sell_with_1" {if 1|in_array:$company_data.do_you_sell_do_not_sell_with}checked="checked"{/if} value="1"  disabled="disabled" />
            {__("sd_vendor_fields_do_you_sell_linio")}
        </label>
        
        <label class="checkbox inline" for="elm_do_you_sell_do_not_sell_with_2">
            <input type="checkbox" name="company_data[do_you_sell_do_not_sell_with][2]" id="elm_do_you_sell_do_not_sell_with_2" {if 2|in_array:$company_data.do_you_sell_do_not_sell_with}checked="checked"{/if} value="2"  disabled="disabled" />
            {__("sd_vendor_fields_do_you_sell_mercadolibre")}
        </label>
        
        <label class="checkbox inline" for="elm_do_you_sell_do_not_sell_with_3">
            <input type="checkbox" name="company_data[do_you_sell_do_not_sell_with][3]" id="elm_do_you_sell_do_not_sell_with_3" {if 3|in_array:$company_data.do_you_sell_do_not_sell_with}checked="checked"{/if} value="3"  disabled="disabled" />
            {__("sd_vendor_fields_do_you_sell_amazon")}
        </label>
        
        <label class="checkbox inline" for="elm_do_you_sell_do_not_sell_with_4">
            <input type="checkbox" name="company_data[do_you_sell_do_not_sell_with][4]" id="elm_do_you_sell_do_not_sell_with_4" {if 4|in_array:$company_data.do_you_sell_do_not_sell_with}checked="checked"{/if} value="4"  disabled="disabled" />
            {__("sd_vendor_fields_do_you_sell_ebay")}
        </label>
        
        <label class="checkbox inline" for="elm_do_you_sell_do_not_sell_with_5">
            <input type="checkbox" name="company_data[do_you_sell_do_not_sell_with][5]" id="elm_do_you_sell_do_not_sell_with_5" {if 5|in_array:$company_data.do_you_sell_do_not_sell_with}checked="checked"{/if} value="5"  disabled="disabled" />
            {__("sd_vendor_fields_do_you_sell_do_not_sell_with")}
        </label>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="can_you_invoice_in_mexico">{__("sd_vendor_fields_can_you_invoice_in_mexico")}:</label>
    <div class="controls">
        <label class="checkbox">
            <input type="hidden" name="company_data[can_you_invoice_in_mexico]" value="N" />
            <input type="checkbox" name="company_data[can_you_invoice_in_mexico]" id="can_you_invoice_in_mexico" value="Y"{if $company_data.can_you_invoice_in_mexico == "Y"}checked="checked"{/if}  disabled="disabled" />
        </label>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="do_you_have_5_or_more_pieces">{__("sd_vendor_fields_do_you_have_5_or_more_pieces")}:</label>
    <div class="controls">
        <label class="checkbox">
            <input type="hidden" name="company_data[do_you_have_5_or_more_pieces]" value="N" />
            <input type="checkbox" name="company_data[do_you_have_5_or_more_pieces]" id="do_you_have_5_or_more_pieces" value="Y"{if $company_data.do_you_have_5_or_more_pieces == "Y"}checked="checked"{/if}  disabled="disabled" />
        </label>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="can_you_ship_48">{__("sd_vendor_fields_can_you_ship_48")}:</label>
    <div class="controls">
        <label class="checkbox">
            <input type="hidden" name="company_data[can_you_ship_48]" value="N" />
            <input type="checkbox" name="company_data[can_you_ship_48]" id="can_you_ship_48" value="Y"{if $company_data.can_you_ship_48 == "Y"}checked="checked"{/if}  disabled="disabled" />
        </label>
    </div>
</div>

<div class="control-group">
    <label for="how_many_products" class="control-label cm-required">{__("sd_vendor_fields_how_many_products")}:</label>
    <div class="controls">
        <input type="text" class="input-medium" name="company_data[how_many_products]" id="how_many_products" size="32" value="{$company_data.how_many_products}"  disabled="disabled" />
    </div>
</div>

<div class="control-group">
    <label for="what_brands_sell" class="control-label cm-required">{__("sd_vendor_fields_what_brands_sell")}:</label>
    <div class="controls">
        <input type="text" class="input-large" name="company_data[what_brands_sell]" id="what_brands_sell" size="32" value="{$company_data.what_brands_sell}"  disabled="disabled" />
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="products_new_and_original">{__("sd_vendor_fields_products_new_and_original")}:</label>
    <div class="controls">
        <label class="checkbox">
            <input type="hidden" name="company_data[products_new_and_original]" value="N" />
            <input type="checkbox" name="company_data[products_new_and_original]" id="products_new_and_original" value="Y"{if $company_data.products_new_and_original == "Y"}checked="checked"{/if}  disabled="disabled" />
        </label>
    </div>
</div>

{include file="common/subheader.tpl" title=__("sd_vendor_fields_legal_vendor_information")}

<div class="control-group">
    <label for="legal_name" class="control-label cm-required">{__("sd_vendor_fields_legal_name")}:</label>
    <div class="controls">
        <input type="text" class="input-large" name="company_data[legal_name]" id="legal_name" size="32" value="{$company_data.legal_name}"  disabled="disabled" />
    </div>
</div>

<div class="control-group">
    <label for="rfc" class="control-label cm-required">{__("sd_vendor_fields_rfc")}:</label>
    <div class="controls">
        <input type="text" class="input-large" name="company_data[rfc]" id="rfc" size="32" value="{$company_data.rfc}" disabled="disabled" />
    </div>
</div>

<div class="control-group">
    <label class="control-label cm-required">{__("sd_vendor_fields_type_of_taxpayer")}:</label>
    <div class="controls">
        <label class="radio inline" for="type_of_taxpayer_1">
            <input type="radio" name="company_data[type_of_taxpayer]" id="type_of_taxpayer_1" checked="checked" value="1"  disabled="disabled"/>
            {__("sd_vendor_fields_personal")}
        </label>
        
        <label class="radio inline" for="type_of_taxpayer_2">
            <input type="radio" name="company_data[type_of_taxpayer]" id="type_of_taxpayer_2" {if $company_data.type_of_taxpayer == '2'}checked="checked"{/if} value="2"  disabled="disabled"/>
            {__("sd_vendor_fields_company")}
        </label>
    </div>
</div>

<div class="control-group">
    <label for="elm_company_li_address" class="control-label cm-required">{__("address")}:</label>
    <div class="controls">
        <input type="text" class="input-large" name="company_data[li_address]" id="elm_company_li_address" size="32" value="{$company_data.li_address}"  disabled="disabled"/>
    </div>
</div>

<div class="control-group">
    <label for="elm_company_li_city" class="control-label cm-required">{__("city")}:</label>
    <div class="controls">
        <input type="text" class="input-large" name="company_data[li_city]" id="elm_company_li_city" size="32" value="{$company_data.li_city}"  disabled="disabled"/>
    </div>
</div>

<div class="control-group">
    <label for="elm_company_li_country" class="control-label cm-required">{__("country")}:</label>
    <div class="controls">
    {assign var="_country" value=$company_data.li_country|default:$settings.General.default_country}
    <select class="cm-country cm-location-billing" id="elm_company_li_country" name="company_data[li_country]"  disabled="disabled" >
        <option value="">- {__("select_country")} -</option>
        {foreach from=$countries item="country" key="code"}
        <option {if $_country == $code}selected="selected"{/if} value="{$code}">{$country}</option>
        {/foreach}
    </select>
    </div>
</div>

<div class="control-group">
    {$_country = $company_data.li_country|default:$settings.General.default_country}
    {$_state = $company_data.li_state|default:$settings.General.default_state}

    <label for="elm_company_li_state" class="control-label cm-required">{__("state")}:</label>
    <div class="controls">
    <select id="elm_company_li_state" name="company_data[li_state]" class="cm-state cm-location-billing {if !$states.$_country}hidden{/if}"  disabled="disabled">
        <option value="">- {__("select_state")} -</option>
        {if $states.$_country}
            {foreach from=$states.$_country item=state}
                <option {if $_state == $state.code}selected="selected"{/if} value="{$state.code}">{$state.state}</option>
            {/foreach}
        {/if}
    </select>
    <input type="text" id="elm_company_li_state_d" name="company_data[li_]" size="32" maxlength="64" value="{$_state}" {if $states.$_country}disabled="disabled"{/if} class="cm-state cm-location-billing {if $states.$_country}hidden{/if} cm-skip-avail-switch" />
    </div>
</div>

<div class="control-group">
    <label for="elm_company_li_zipcode" class="control-label cm-required cm-zipcode cm-location-billing">{__("zip_postal_code")}:</label>
    <div class="controls">
        <input type="text" name="company_data[li_zipcode]" id="elm_company_li_zipcode" size="32" value="{$company_data.li_zipcode}"  disabled="disabled"/>
    </div>
</div>

<div class="control-group">
    <label for="legal_representative_name" class="control-label cm-required">{__("sd_vendor_fields_legal_representative_name")}:</label>
    <div class="controls">
        <input type="text" class="input-large" name="company_data[legal_representative_name]" id="legal_representative_name" size="32" value="{$company_data.legal_representative_name}"  disabled="disabled"/>
    </div>
</div>

<div class="control-group ">
    <label for="type_{"legal_frc_file[`$id`]"|md5}" class="control-label {if !$company_data.company_attachments.legal_frc_file}cm-required{/if}" >{__("sd_vendor_fields_legal_frc_file")}</label>
    <div class="controls">
        {if $company_data.company_attachments.legal_frc_file}
            <div id="box_attach_images_legal_frc_file">
                <a href="{"companies.get_file?attachment_id=`$company_data.company_attachments.legal_frc_file.attachment_id`"|fn_url}">{$company_data.company_attachments.legal_frc_file.filename}</a> ({$company_data.company_attachments.legal_frc_file.filesize|formatfilesize nofilter})
            </div>
        {/if}
        {include file="common/fileuploader.tpl" var_name="legal_frc_file[`$id`]"}
    </div>
</div>

<div class="control-group ">
    <label for="type_{"legal_efe_file[`$id`]"|md5}" class="control-label {if !$company_data.company_attachments.legal_efe_file}cm-required{/if}">{__("sd_vendor_fields_legal_efe_file")}</label>
    <div class="controls">
        {if $company_data.company_attachments.legal_efe_file}
            <div id="box_attach_images_legal_efe_file">
                <a href="{"companies.get_file?attachment_id=`$company_data.company_attachments.legal_efe_file.attachment_id`"|fn_url}">{$company_data.company_attachments.legal_efe_file.filename}</a> ({$company_data.company_attachments.legal_efe_file.filesize|formatfilesize nofilter})
            </div>
        {/if}
        {include file="common/fileuploader.tpl" var_name="legal_efe_file[`$id`]"}
    </div>
</div>

<div class="control-group ">
    <label for="type_{"address_conf_file[`$id`]"|md5}" class="control-label {if !$company_data.company_attachments.address_conf_file}cm-required{/if}">{__("sd_vendor_fields_address_conf_file")}</label>
    <div class="controls">
        {if $company_data.company_attachments.address_conf_file}
            <div id="box_attach_images_address_conf_file">
                <a href="{"companies.get_file?attachment_id=`$company_data.company_attachments.address_conf_file.attachment_id`"|fn_url}">{$company_data.company_attachments.address_conf_file.filename}</a> ({$company_data.company_attachments.address_conf_file.filesize|formatfilesize nofilter})
            </div>
        {/if}
        {include file="common/fileuploader.tpl" var_name="address_conf_file[`$id`]"}
    </div>
</div>

{include file="common/subheader.tpl" title=__("sd_vendor_fields_banking_information")}

<div class="control-group">
    <label for="account_owner" class="control-label cm-required">{__("sd_vendor_fields_account_owner")}:</label>
    <div class="controls">
        <input type="text" class="input-large" name="company_data[account_owner]" id="account_owner" size="32" value="{$company_data.account_owner}"  disabled="disabled"/>
    </div>
</div>

<div class="control-group">
    <label for="account_number" class="control-label cm-required">{__("sd_vendor_fields_account_number")}:</label>
    <div class="controls">
        <input type="text" class="input-large" name="company_data[account_number]" id="account_number" size="32" value="{$company_data.account_number}"  disabled="disabled"/>
    </div>
</div>

<div class="control-group">
    <label for="bank" class="control-label cm-required">{__("sd_vendor_fields_bank")}:</label>
    <div class="controls">
        <input type="text" class="input-large" name="company_data[bank]" id="bank" size="32" value="{$company_data.bank}"  disabled="disabled"/>
    </div>
</div>

<div class="control-group">
    <label for="clabe" class="control-label cm-required">{__("sd_vendor_fields_clabe")}:</label>
    <div class="controls">
        <input type="text" class="input-large" name="company_data[clabe]" id="clabe" size="32" value="{$company_data.clabe}"  disabled="disabled"/>
    </div>
</div>

<div class="control-group">
    <label for="swift" class="control-label cm-required">{__("sd_vendor_fields_swift")}:</label>
    <div class="controls">
        <input type="text" class="input-large" name="company_data[swift]" id="swift" size="32" value="{$company_data.swift}"  disabled="disabled"/>
    </div>
</div>

<div class="control-group">
    <label for="iban" class="control-label">{__("sd_vendor_fields_iban")}:</label>
    <div class="controls">
        <input type="text" class="input-large" name="company_data[iban]" id="iban" size="32" value="{$company_data.iban}"  disabled="disabled"/>
    </div>
</div>

<div class="control-group ">
    <label for="type_{"address_account_statement[`$id`]"|md5}" class="control-label {if !$company_data.company_attachments.address_account_statement}cm-required{/if}">{__("sd_vendor_fields_account_statement")}</label>
    <div class="controls">
        {if $company_data.company_attachments.address_account_statement}
            <div id="box_attach_images_address_account_statement">
                <a href="{"companies.get_file?attachment_id=`$company_data.company_attachments.address_account_statement.attachment_id`"|fn_url}">{$company_data.company_attachments.address_account_statement.filename}</a> ({$company_data.company_attachments.address_account_statement.filesize|formatfilesize nofilter})              
            </div>
        {/if}
        {include file="common/fileuploader.tpl" var_name="address_account_statement[`$id`]"}
    </div>
</div>


<script type="text/javascript">
(function(_, $) {
    $.ceEvent('on', 'ce.delete_attachment', function(r, p) {
        if (r.deleted == true) {
            $('#' + p.result_ids).remove();
        }        
    });    
}(Tygh, Tygh.$));    
</script>
{/if}