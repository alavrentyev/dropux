<div class="control-group">
    <label class="control-label" for="elm_package_contents">{__("package_contents")}:</label>
    <div class="controls">
        <input type="text" name="product_data[package_contents]" id="elm_package_contents" value="{$product_data.package_contents}" class="input-long" />
    </div>
</div>
    
<div class="control-group">
    <label class="control-label" for="elm_primary_color">{__("primary_color")}:</label>
    <div class="controls">
        <input type="text" name="product_data[primary_color]" id="elm_primary_color" value="{$product_data.primary_color}" class="input-long" />
    </div>
</div>
    
<div class="control-group">
    <label class="control-label" for="elm_warranty_months">{__("warranty_months")}:</label>
    <div class="controls">
        <input type="text" name="product_data[warranty_months]" size="10" id="elm_warranty_months" value="{$product_data.warranty_months}" class="input-small" />
    </div>
</div>
    
<div class="control-group">
    <label class="control-label" for="elm_warranty_description">{__("warranty_description")}:</label>
    <div class="controls">
        <textarea id="elm_warranty_description" name="product_data[warranty_description]" cols="55" rows="2" class="cm-wysiwyg input-large">{$product_data.warranty_description}</textarea>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="elm_country_origin">{__("country_origin")}:</label>
    <div class="controls">
        <input type="text" name="product_data[country_origin]" id="elm_country_origin" value="{$product_data.country_origin}" class="input-long" />
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="elm_product_condition">{__("product_condition")}:</label>
    <div class="controls">
        <select class="span3" name="product_data[product_condition]" id="elm_product_condition">
            <option value="N" {if $product_data.product_condition == "N"}selected="selected"{/if}>{__("pc_new")}</option>
            <option value="R" {if $product_data.product_condition == "R"}selected="selected"{/if}>{__("pc_refurbished")}</option>
        </select>
    </div>
</div>